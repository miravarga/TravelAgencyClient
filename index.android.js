/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import {packReducer} from './src/pack';
import {authReducer} from './src/auth';
import {Router} from './src/Router'

const rootReducer = combineReducers({pack: packReducer, auth: authReducer});
const store = createStore(rootReducer, applyMiddleware(thunk, createLogger({colors: {}})));
// const store = createStore(rootReducer, applyMiddleware(thunk));

export default class K10 extends Component {
  render() {
    return (
      <Router store={store}/>
    );
  }
}

AppRegistry.registerComponent('K10', () => K10);
