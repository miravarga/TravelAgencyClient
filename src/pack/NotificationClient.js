const io = require('socket.io-client/socket.io');
import {serverUrl} from '../core/api';
import {getLogger} from '../core/utils';
import {packCreated, packUpdated, packDeleted} from './service';

window.navigator.userAgent = 'ReactNative';

const log = getLogger('NotificationClient');

const PACK_CREATED = 'pack/created';
const PACK_UPDATED = 'pack/updated';
const PACK_DELETED = 'pack/deleted';

export class NotificationClient {
  constructor(store) {
    this.store = store;
  }

  connect() {
    log(`connect...`);
    const store = this.store;
    const auth = store.getState().auth;
    this.socket = io(auth.server.url, {transports: ['websocket']});
    const socket = this.socket;
    socket.on('connect', () => {
      log('connected');
      socket
        .emit('authenticate', {token: auth.token})
        .on('authenticated', () => log(`authenticated`))
        .on('unauthorized', (msg) => log(`unauthorized: ${JSON.stringify(msg.data)}`))
    });
    socket.on(PACK_CREATED, (pack) => {
      log(PACK_CREATED);
      store.dispatch(packCreated(pack));
    });
    socket.on(PACK_UPDATED, (pack) => {
      log(PACK_UPDATED);
      store.dispatch(packUpdated(pack))
    });
    socket.on(PACK_DELETED, (pack) => {
      log(PACK_DELETED);
      store.dispatch(packDeleted(pack))
    });
  };

  disconnect() {
    log(`disconnect`);
    this.socket.disconnect();
  }
}