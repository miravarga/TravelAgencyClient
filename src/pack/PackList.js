import React, {Component} from 'react';
import {ListView, Text, View, StatusBar, ActivityIndicator} from 'react-native';
import {PackEdit} from './PackEdit';
import {packView} from './PackView';
import {loadpacks, cancelLoadpacks} from './service';
import {registerRightAction, getLogger, issueToText} from '../core/utils';
import styles from '../core/styles';

const log = getLogger('PackList');
const PACK_LIST_ROUTE = 'pack/list';

export class PackList extends Component {
  static get routeName() {
    return PACK_LIST_ROUTE;
  }

  static get route() {
    return {name: PACK_LIST_ROUTE, title: 'Pack List', rightText: 'New'};
  }

  constructor(props) {
    super(props);
    log('constructor');
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.id !== r2.id});
    this.store = this.props.store;
    const packState = this.store.getState().pack;
    this.state = {isLoading: packState.isLoading, dataSource: this.ds.cloneWithRows(packState.items)};
    registerRightAction(this.props.navigator, this.onNewpack.bind(this));
  }

  render() {
    log('render');
    let message = issueToText(this.state.issue);
    return (
      <View style={styles.content}>
        { this.state.isLoading &&
        <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
        }
        {message && <Text>{message}</Text>}
        <ListView
          dataSource={this.state.dataSource}
          enableEmptySections={true}
          renderRow={pack => (<packView pack={pack} onPress={(pack) => this.onpackPress(pack)}/>)}/>
      </View>
    );
  }

  onNewpack() {
    log('onNewpack');
    this.props.navigator.push({...PackEdit.route});
  }

  onpackPress(pack) {
    log('onpackPress');
    this.props.navigator.push({...PackEdit.route, data: pack});
  }

  componentDidMount() {
    log('componentDidMount');
    this._isMounted = true;
    const store = this.store;
    this.unsubscribe = store.subscribe(() => {
      log('setState');
      const packState = store.getState().pack;
      this.setState({dataSource: this.ds.cloneWithRows(packState.items), isLoading: packState.isLoading});
    });
    store.dispatch(loadpacks());
  }

  componentWillUnmount() {
    log('componentWillUnmount');
    this._isMounted = false;
    this.unsubscribe();
    if (this.state.isLoading) {
      this.store.dispatch(cancelLoadpacks());
    }
  }
}
