import {action, getLogger, errorPayload} from '../core/utils';
import {search, save} from './resource';

const log = getLogger('pack/service');

// Loading packs
const LOAD_PACKS_STARTED = 'pack/loadStarted';
const LOAD_PACKS_SUCCEEDED = 'pack/loadSucceeded';
const LOAD_PACKS_FAILED = 'pack/loadFailed';
const CANCEL_LOAD_PACKS = 'pack/cancelLoad';

// Saving packs
const SAVE_PACK_STARTED = 'pack/saveStarted';
const SAVE_PACK_SUCCEEDED = 'pack/saveSucceeded';
const SAVE_PACK_FAILED = 'pack/saveFailed';
const CANCEL_SAVE_PACK = 'pack/cancelSave';

// Pack notifications
const PACK_DELETED = 'pack/deleted';

export const loadPacks = () => async(dispatch, getState) => {
  log(`loadPacks...`);
  const state = getState();
  const packState = state.pack;
  try {
    dispatch(action(LOAD_PACKS_STARTED));
    const packs = await search(state.auth.server, state.auth.token)
    log(`loadPacks succeeded`);
    if (!packState.isLoadingCancelled) {
      dispatch(action(LOAD_PACKS_SUCCEEDED, packs));
    }
  } catch(err) {
    log(`loadPacks failed`);
    if (!packState.isLoadingCancelled) {
      dispatch(action(LOAD_PACKS_FAILED, errorPayload(err)));
    }
  }
};

export const cancelLoadPacks = () => action(CANCEL_LOAD_PACKS);

export const savePack = (pack) => async(dispatch, getState) => {
  log(`savePack...`);
  const state = getState();
  const packState = state.pack;
  try {
    dispatch(action(SAVE_PACK_STARTED));
    const savedPack = await save(state.auth.server, state.auth.token, pack)
    log(`savePack succeeded`);
    if (!packState.isSavingCancelled) {
      dispatch(action(SAVE_PACK_SUCCEEDED, savedPack));
    }
  } catch(err) {
    log(`savePack failed`);
    if (!packState.isSavingCancelled) {
      dispatch(action(SAVE_PACK_FAILED, errorPayload(err)));
    }
  }
};

export const cancelSavePack = () => action(CANCEL_SAVE_PACK);

export const packCreated = (createdPack) => action(SAVE_PACK_SUCCEEDED, createdPack);
export const packUpdated = (updatedPack) => action(SAVE_PACK_SUCCEEDED, updatedPack);
export const packDeleted = (deletedPack) => action(PACK_DELETED, deletedPack);

export const packReducer = (state = {items: [], isLoading: false, isSaving: false}, action) => { //newState (new object)
  let items, index;
  switch (action.type) {
    // Loading
    case LOAD_PACKS_STARTED:
      return {...state, isLoading: true, isLoadingCancelled: false, issue: null};
    case LOAD_PACKS_SUCCEEDED:
      return {...state, items: action.payload, isLoading: false};
    case LOAD_PACKS_FAILED:
      return {...state, issue: action.payload.issue, isLoading: false};
    case CANCEL_LOAD_PACKS:
      return {...state, isLoading: false, isLoadingCancelled: true};
    // Saving
    case SAVE_PACK_STARTED:
      return {...state, isSaving: true, isSavingCancelled: false, issue: null};
    case SAVE_PACK_SUCCEEDED:
      items = [...state.items];
      index = items.findIndex((i) => i._id == action.payload._id);
      if (index != -1) {
        items.splice(index, 1, action.payload);
      } else {
        items.push(action.payload);
      }
      return {...state, items, isSaving: false};
    case SAVE_PACK_FAILED:
      return {...state, issue: action.payload.issue, isSaving: false};
    case CANCEL_SAVE_PACK:
      return {...state, isSaving: false, isSavingCancelled: true};
    // Notifications
    case PACK_DELETED:
      items = [...state.items];
      const deletedPack = action.payload;
      index = state.items.findIndex((pack) => pack._id == deletedPack._id);
      if (index != -1) {
        items.splice(index, 1);
        return {...state, items};
      }
      return state;
    default:
      return state;
  }
};