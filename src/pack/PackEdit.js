import React, {Component} from 'react';
import {Text, View, TextInput, ActivityIndicator} from 'react-native';
import {savepack, cancelSavepack} from './service';
import {registerRightAction, issueToText, getLogger} from '../core/utils';
import styles from '../core/styles';

const log = getLogger('PackEdit');
const PACK_EDIT_ROUTE = 'travelPackages/edit';

export class PackEdit extends Component {
  static get routeName() {
    return PACK_EDIT_ROUTE;
  }

  static get route() {
    return {name: PACK_EDIT_ROUTE, title: 'Pack Edit', rightText: 'Save'};
  }

  constructor(props) {
    log('constructor');
    super(props);
    this.store = this.props.store;
    const nav = this.props.navigator;
    this.navigator = nav;
    const currentRoutes = nav.getCurrentRoutes();
    const currentRoute = currentRoutes[currentRoutes.length - 1];
    if (currentRoute.data) {
      this.state = {pack: {...currentRoute.data}, isSaving: false};
    } else {
      this.state = {pack: {text: ''}, isSaving: false};
    }
    registerRightAction(nav, this.onSave.bind(this));
  }

  render() {
    log('render');
    const state = this.state;
    let message = issueToText(state.issue);
    return (
      <View style={styles.content}>
        { state.isSaving &&
        <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
        }
        <Text>Text</Text>
        <TextInput value={state.pack.text} onChangeText={(text) => this.updatepackText(text)}></TextInput>
        {message && <Text>{message}</Text>}
      </View>
    );
  }

  componentDidMount() {
    log('componentDidMount');
    this._isMounted = true;
    const store = this.props.store;
    this.unsubscribe = store.subscribe(() => {
      log('setState');
      const state = this.state;
      const packState = store.getState().pack;
      this.setState({...state, issue: packState.issue});
    });
  }

  componentWillUnmount() {
    log('componentWillUnmount');
    this._isMounted = false;
    this.unsubscribe();
    if (this.state.isLoading) {
      this.store.dispatch(cancelSavepack());
    }
  }

  updatepackText(text) {
    let newState = {...this.state};
    newState.pack.text = text;
    this.setState(newState);
  }

  onSave() {
    log('onSave');
    this.store.dispatch(savepack(this.state.pack)).then(() => {
      log('onpackSaved');
      if (!this.state.issue) {
        this.navigator.pop();
      }
    });
  }
}